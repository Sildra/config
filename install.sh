#!/bin/bash
# Install script for configuration files
# Realised by Thibault "Sildra" FRESNET
# array format is ( "src dst bkup" )

conf=$PWD/config
backup=$HOME/backup
verbose=
force=0

func=default

function default
{
    backup $1
    install $1
}

function backup
{
    echo -e "\e[31mCreating backup for $1's configuration\e[0m"
    mkdir -p $backup/$1
    for elt in "${array[@]}"; do
        for ind in $elt; do
            src=$dst
            dst=$bkup
            bkup=$ind
        done
        if [ -L $src ]; then
            echo "Skipping symbolic link for backup source"
        else
            # REMOVE BACKUP FILE (might be improved but functional)
            if [ -e $bkup ]; then
                echo "Warning : previous backup file exists"
                answer=n
                if [ $force = 0 ]; then
                    echo -n "Remove previous file ? [Y/n] "
                    read answer
                else
                    answer=y
                fi
                case $answer in
                    y|Y|yes|YES)
                        rm -rf $bkup
                        ;;
                    *)
                        if [ -z $answer ]; then
                            rm -rf $bkup
                        fi
                esac
            fi
            mv -n $verbose $dst $bkup
        fi
    done
}

function restore
{
    echo -e "\e[34mRestoring $1's configuration\e[0m"
    for elt in "${array[@]}"; do
        for ind in $elt; do
            src=$dst
            dst=$bkup
            bkup=$ind
        done
        # REMOVE DESTINATION FILE (might be improved but functional)
        if [ -e $bkup ]; then
            if [ -e $dst ]; then
                echo "Warning : destination file exists"
                answer=n
                if [ $force = 0 ]; then
                    echo -n "Remove destination file ? [Y/n] "
                    read answer
                else
                    answer=y
                fi
                case $answer in
                    y|Y|yes|YES)
                        rm -rf $dst
                        ;;
                    *)
                        if [ -z $answer ]; then
                            rm -rf $dst
                        fi
                esac
            fi
        fi
        mv -n $verbose $bkup $dst
    done
}

function install
{
    echo -e "\e[32mInstalling $1's configuration\e[0m"
    for elt in "${array[@]}"; do
        for ind in $elt; do
            src=$dst
            dst=$bkup
            bkup=$ind
        done
        if [ -h $dst ]; then
            echo "$dst is a symlink, no changes mas"
        elif [ -d $dst ]; then
            echo "$dst is a directory, no changes made"
        else
            ln -fs $src $dst
        fi
    done
}

function display
{
    echo -e "\e[32mDummy installation of $1\e[0m"
    for elt in "${array[@]}"; do
        for ind in $elt; do
            src=$dst
            dst=$bkup
            bkup=$ind
        done
        echo "ln -fs $src $dst"
    done
}

function run
{
    for project in $1; do
        case $project in
            zsh)
                array=( "$conf/zsh/zshrc $HOME/.zshrc $backup/zsh/zshrc"
                "$conf/zsh/zsh $HOME/.config/zsh $backup/zsh/zsh"
                "$conf/shell-config $HOME/.config/shell-config
                $backup/shell-config/shell-config" )
                $func zsh
                ;;
            bash)
                array=( "$conf/bash/bashrc $HOME/.bashrc $backup/bash/bashrc"
                "$conf/bash/bash $HOME/.config/bash $backup/bash/bash"
                "$conf/shell-config $HOME/.config/shell-config
                $backup/shell-config/shell-config" )
                $func bash
                ;;
            xfiles)
                array=( "$conf/xfiles/Xdefaults $HOME/.Xdefaults
                $backup/xfiles/Xdefaults"
                "$conf/xfiles/xinitrc $HOME/.xinitrc $backup/xfiles/xinitrc")
                $func xfiles
                ;;
            pandoc)
                array=( "$conf/pandoc $HOME/.pandoc $backup/pandoc" )
                $func pandoc
                ;;
            vimpager)
                array=( "$conf/vimpager/vimpagerrc $HOME/.vimpagerrc
                $backup/vimpager/vimpagerrc" )
                $func vimpager
                ;;
            tmux)
                array=( "$conf/tmux/tmux.conf $HOME/.tmux.conf
                $backup/tmux/tmux.conf" )
                $func tmux
                ;;
            ncmpcpp)
                array=( "$conf/ncmpcpp/config $HOME/.ncmpcpp/config
                $backup/ncmpcpp/config" )
                $func ncmpcpp
                ;;
            git)
                array=( "$conf/git/gitignore $HOME/.gitignore
                $backup/git/gitignore" )
                $func git
                ;;
            i3)
                array=( "$conf/i3 $HOME/.i3 $backup/i3" )
                $func i3
                ;;
            pandoc)
                array=( "$conf/pandoc $HOME/.pandoc $backup/pandoc" )
                $func pandoc
                ;;
            vim)
                array=( "$conf/vim/vim $HOME/.vim $backup/vim/vim"
                "$conf/vim/vimrc $HOME/.vimrc $backup/vim/vimrc" )
                $func vim
                ;;
            bin)
                echo -e "\e[32mInstalling bin's directory\e[0m"
                echo -e "If you're not using any shell config provided," \
                    "include $conf/bin to your PATH"
                if [ -d $HOME/.config ]; then
                    grep "PATH+=:$conf/bin$" $HOME/.config/extra-paths.sh &> \
                    /dev/null
                    if [ $? -ne 0 ]; then
                        echo -e "echo \$PATH | grep \"$conf/bin\"" \
                            "&> /dev/null\n" \
                            "if [ \$? = 1 ]; then\n" \
                            "    PATH+=:$conf/bin\nfi" \
                            >> $HOME/.config/extra-paths.sh
                        echo "Bin directory added in PATH"
                    else
                        echo "Folder already referenced in configuration"
                    fi
                else
                    echo "Error : $HOME/.config is not a directory."
                fi
                ;;
            *)
                echo
                echo "Unknown argument $project"
                echo "Run \"$0 help\" to display the help"
                exit 1
                ;;
        esac
    done

}

if [ $# = 0 ]; then
    run "zsh bash xdefault"
fi

for arg in $@; do
    case $arg in
        # OPTION SECTION
        help|-h)
            echo "Install script for configuration files"
            echo "Usage : $0 [any_of_the_1st_section]"\
                " [one_of_the_2nd_section] [any_of_the_3rd_secton]"
            echo
            echo "    help     : display help (this message)"
            echo "    verbose  : print verbose messages"
            echo "    force    : force some operations"
            echo
            echo "    default  : perform backup before install"
            echo "    install  : install following configurations"
            echo "    backup   : backup files that can be erased by the" \
                 "configuration files"
            echo "    restore  : restore the backup files"
            echo
            echo "    all      : all the above configuration files"
            echo "    zsh      : Z-Shell configuration files"
            echo "    bash     : Bash configuration files"
            echo "    ncmpcpp  : ncmpcpp curses MPD player configuration files"
            echo "    vimpager : vim PAGER configuration files"
            echo "    xfiles   : Xdefault/xinitrc file"
            echo "    i3       : i3-wm configuration files/scripts"
            echo "    tmux     : tmux configuration"
            echo "    git      : git configuration"
            echo "    pandoc   : pandoc personalized headers"
            echo "    bin      : Add bin directory to PATH" \
                 "(file : \$HOME/.config/extra-paths)"
            exit 0
            ;;
        verbose)
            verbose=-v
            ;;
        force)
            force=1
            ;;
        # COMPORTMENT SECTION
        default)
            func=default
            ;;
        install)
            func=install
            ;;
        backup)
            func=backup
            ;;
        restore)
            func=restore
            ;;
        display)
            func=display
            ;;
        # PROJECT SECTION
        all)
            run "zsh bash ncmpcpp vimpager xfiles i3 git bin pandoc"
            ;;
        *)
            run $arg
    esac
done

