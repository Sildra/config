INSTALLATION
============
Each part of the project can be installed separately following the script
options:

    ./install.sh install zsh

A backup/restore system is provided for easy testing:

    ./install.sh backup zsh
    ./install.sh restore zsh

By default, the script creates a backup before installing files:

    ./install.sh zsh

Equivalent:

    ./install.sh default zsh

All the configurations can be installed at once using all:

    ./install.sh all

For further details:

    ./install.sh help

REQUIREMENTS
============
The zsh's prompt heavily relies on a 256color terminal and an UTF-8 compliant
font.
I recommand the use of URxvt and terminusmodx UTF-8 patched font available from
[Remi "Halfr" AUDEBERT](https://bitbucket.org/halfr/terminusmodx)

AUTHORS
=======
Compilation work from [Thibault "Sildra"
FRESNET](https://bitbucket.org/Sildra/config)

bash/zsh initial work is from [Pierre "Delroth"
BOURDON](https://bitbucket.org/delroth/configs)

vim-based configurations from [Kevin "Chewie" SZTERN](https://github.com/Chewie/configs)

Any file has his own AUTHORS
