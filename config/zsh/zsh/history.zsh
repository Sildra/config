# history.zsh
# History related parameters

# Author Pierre "Delroth" BOURDON

HISTFILE=~/.history_zsh
HISTSIZE=10000
SAVEHIST=100000

unsetopt sharehistory
setopt appendhistory
