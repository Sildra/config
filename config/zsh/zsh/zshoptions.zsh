# zshoptions.zsh
# ZSH global options.
# Author Pierre "Delroth" BOURDON

setopt autocd
setopt autocontinue
setopt autolist
setopt automenu
setopt autoparamkeys
setopt autoparamslash
setopt autopushd
setopt correct
unsetopt flow_control
setopt multios
setopt notify
setopt numericglobsort
setopt pushdsilent
setopt pushdtohome
setopt recexact
