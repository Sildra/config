# rl.zsh
# Adds a rl command to ZSH
# Author Pierre "Delroth" BOURDON

function rl {
    hash -r
    source ~/.zshrc
}
