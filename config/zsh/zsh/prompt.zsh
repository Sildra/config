# prompt.zsh
# My powerline 256C multi-line zsh prompt.
# Made by Thibault 'Sildra' FRESNET
#
# Spetial thanks to Alexis 'Horgix' CHOTARD and the vim/powerline development
# team who greatly inspired me.

function _prompt_precmd ()
{
    vcs_info

    # If we have a ssh connection on the process tree, we display the hostname
    pstree -s $$ | grep sshd &> /dev/null
    if [ $? -ne 0 ] && [ -z "$SSH_CLIENT" ]; then
        local pr_h="$bbk2$fcl$reset"
        local pr_offset_host=37
    else
        # Background orange
        local pr_h="$bo$fcl$reset$bo$fbk2 @$HOST $fo$bbk2$fcl$reset"
        local pr_offset_host=85
    fi

    local TERMWIDTH=$(($COLUMNS + $prompt_offset + $pr_offset_host))


    local pr_u="$fgra$bdr %n $fdr"
    local pr_p="$fyl$bbk2 %~ $ecl$reset"
    local pr_d="$fcy$bbk2 %* $ecl$reset"
    #local pr_git="$fvl$bbk2$ecr ${ref/refs\/heads\//⭠ }${vcs_info_msg_0_}$reset"
    local pr_git="$fvl$bbk2$ecr ${vcs_info_msg_0_}$reset"

    PROMPT_LEFT=$pr_u$pr_h$pr_p$pr_d
    PROMPT_RIGHT=$pr_git

    local WD_LEFT=${#${(%):-%n$pr_h%~%*}}
    local WD_RIGHT=${#${(%):-${vcs_info_msg_0_}}}

    FILL_BAR="$bbk2\${(l.(($TERMWIDTH - ($WD_LEFT + $WD_RIGHT))).. .)}$reset"

    if [ $WD_RIGHT -lt 5 ]; then
        FILL_BAR=""
        PROMPT_RIGHT=""
    fi
}

function prompt_load_vcs_infos ()
{
    local ref dirty
    if $(git rev-parse --is-inside-work-tree >/dev/null 2>&1); then
        #dirty=$(parse_git_dirty)
        ref=$(git symbolic-ref HEAD 2> /dev/null) || ref="➦ $(git show-ref --head -s --abbrev |head -n1 2> /dev/null)"
        #if [[ -n $dirty ]]; then
        #    prompt_segment yellow black
        #else
        #    prompt_segment green black
        rfi

        setopt promptsubst
        autoload -Uz vcs_info

        zstyle ':vcs_info:*' enable git
        zstyle ':vcs_info:*' get-revision true
        zstyle ':vcs_info:*' check-for-changes true
        zstyle ':vcs_info:*' stagedstr '+'
        zstyle ':vcs_info:git:*' unstagedstr '●'
        zstyle ':vcs_info:*' formats ' %u%c'
        zstyle ':vcs_info:*' actionformats '%u%c'
        vcs_info
    fi

}
function prompt_load_vcs_infos ()
{
    # VCS Support
    autoload -Uz vcs_info
    # Format the vcs infos to only get the git branch
    zstyle ':vcs_info:*' get-revision true
    zstyle ':vcs_info:*' check-for-changes true
    zstyle ':vcs_info:*' stagedstr '✚ '
    zstyle ':vcs_info:git:*' unstagedstr '● '
    zstyle ':vcs_info:*' actionformats \
        '%F{13}%F{13}%b%F{11}|%F{9}%a%F{11}%f'
    zstyle ':vcs_info:*' formats \
        '%b '$fgr'%c'$frd'%u'$fyl$fcr$fbk2$byl' %r '
    zstyle ':vcs_info:*' enable git hg
}

function prompt_set_256_colors ()
{
    # Resets all styles
    reset=$'%{\e[0m%}'

    fyl=$'%{\e[38;5;178m%}' # foreground yellow
    byl=$'%{\e[48;5;178m%}' # background yellow

    fo=$'%{\e[38;5;203m%}' # foreground orange
    bo=$'%{\e[48;5;203m%}' # background orange

    fvl=$'%{\e[38;5;177m%}' # foreground violet
    # bvl=$'%{\e[48;5;177m%}' # background violet

    fcy=$'%{\e[38;5;33m%}' # foreground cyan
    # bcy=$'%{\e[48;5;33m%}' # background cyan

    # If we have a substitued user, change user color
    pstree -s $$ | grep su &> /dev/null
    if [ $? -ne 0 ] || [ -z "$SUDO_USER" ]; then
        fdr=$'%{\e[38;5;52m%}' # foreground dark red
        bdr=$'%{\e[48;5;52m%}' # background dark red
    else
        fdr=$'%{\e[38;5;53m%}' # foreground dark red
        bdr=$'%{\e[48;5;53m%}' # background dark red
    fi

    frd=$'%{\e[38;5;196m%}' # foreground red
    brd=$'%{\e[48;5;196m%}' # background red

    fbk=$'%{\e[38;5;233m%}' # foreground black
    bbk=$'%{\e[48;5;233m%}' # background black

    fbk2=$'%{\e[38;5;234m%}' # foreground black2
    bbk2=$'%{\e[48;5;234m%}' # background black2

    fgra=$'%{\e[38;5;245m%}' # foreground gray
    bgra=$'%{\e[48;5;245m%}' # background gray

    fgr=$'%{\e[38;5;46m%}' # foreground green
    # bgr=$'%{\e[48;5;46m%}' # background green

    prompt_offset=22

    vcs_info
}

function prompt_set_utf_8 ()
{
    # UTF-8 chevron chars :
    fcl=$(echo -e '\0342\0256\0200') # filled chevron left
    ecl=$(echo -e '\0342\0256\0201') # empty chevron left
    fcr=$(echo -e '\0342\0256\0202') # filled chevron right
    ecr=$(echo -e '\0342\0256\0203') # empty chevron right

    vcs_info
}


function prompt_set_fancy ()
{
    # Fancy chevron chars :
    fcl="" # filled chevron left
    ecl=">" # empty chevron left
    fcr="" # filled chevron right
    ecr="<" # empty chevron right

    vcs_info
}

function prompt_set_8_colors ()
{
    # Resets all styles
    reset=$'%{\e[0m%}'

    fyl=$'%{\e[33m%}' # foreground yellow
    byl=$'%{\e[43m%}' # background yellow

    fo=$'%{\e[33m%}' # foreground orange
    bo=$'%{\e[43m%}' # background orange

    fvl=$'%{\e[35m%}' # foreground violet
    # bvl=$'%{\e[48;5;177m%}' # background violet

    fcy=$'%{\e[36m%}' # foreground cyan
    # bcy=$'%{\e[48;5;33m%}' # background cyan

    fdr=$'%{\e[31m%}' # foreground dark red
    bdr=$'%{\e[41m%}' # background dark red

    frd=$'%{\e[31m%}' # foreground red
    brd=$'%{\e[41m%}' # background red

    fbk=$'%{\e[30m%}' # foreground black
    bbk= # background black

    fbk2=$'%{\e[30m%}' # foreground black2
    bbk2=$'%{\e[40m%}' # background black2

    fgra=$'%{\e[39m%}' # foreground gray
    bgra=$'%{\e[49m%}' # background gray

    fgr=$'%{\e[32m%}' # foreground green
    # bgr=$'%{\e[48;5;46m%}' # background green

    pstree -s $$ | grep sshd &> /dev/null
    if [ $? -ne 0 ] && [ -z "$SSH_CLIENT" ]; then
        prompt_offset=-2
    else
        prompt_offset=-26
    fi

    vcs_info
}

function prompt_set_colors ()
{
    autoload colors zsh/terminfo
    prompt_load_vcs_infos

    if [ "$terminfo[colors]" = 256 ]; then
        prompt_set_256_colors
        # Because finding an utf8 compliant font for windows is boring
        if [ $TERM = "putty-256color" ]; then
            prompt_set_fancy
        else
            prompt_set_utf_8
        fi
    else
        prompt_set_8_colors
        prompt_set_fancy
    fi
    prompt_set_fancy
}

function set_prompt ()
{
    prompt_set_colors

    # same as above with inverted color

    # export PROMPT="$prompt_username$prompt_path$prompt_date$prompt_err$prompt_newl"
    export PROMPT='$PROMPT_LEFT${(e)FILL_BAR}$PROMPT_RIGHT
$fgr$bbk%(?..$frd%?)$ecl$reset '
}

add-zsh-hook precmd _prompt_precmd
set_prompt
