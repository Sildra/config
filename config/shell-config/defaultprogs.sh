# defaultprogs.zsh
# Defines the default programs to use.
# Credits to Pierre "Delroth" BOURDON

export EDITOR=vim

[ -x "`which more`" ] && export PAGER=more
[ -x "`which less`" ] && export PAGER=less
[ -x "`which most`" ] && export PAGER=most
[ -x "`which vimpager`" ] && export PAGER=vimpager
