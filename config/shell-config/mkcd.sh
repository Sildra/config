# mkcd.zsh
# A mkcd function which mkdir + cd into a directory.
# Credits to Pierre "Delroth" BOURDON

function mkcd {
    nocorrect mkdir -p "$1" && cd "$1"
}
