# aliases.zsh
# Commonly used aliases
# Author Thibault "Sildra" FRESNET
# Credits to Pierre "Delroth" BOURDON

# Epita style
alias valgrind='valgrind --leak-check=full --track-origins=yes\
    --show-reachable=yes --track-fds=yes --malloc-fill=42 --free-fill=69 -v'

alias grep="grep -R -I"
alias grep="grep --color"

alias cp="nocorrect cp"
alias mkdir="nocorrect mkdir"
alias mv="nocorrect mv"

alias tmux="tmux attach-session; if [ \$? = 1 ]; then tmux; fi;"

# Part of the Generic Makefile's system
alias cpmake="cp -r /home/sildra/dev/templates/* ."
# Part of the Go aliasing directory project (check bin folder)
alias go="source go.sh"

alias mpc="mpc -f \"[[%artist% - ]&[%title%]]|[%file%]\""
alias x="startx & vlock"
alias weechat="weechat-curses -r \"/connect -all\""

# Common option for listing directories
# --du might not be implemented for your distro
alias tree="tree -shvCF --dirsfirst --du"
alias lla="ls -la"
alias la="ls -a"
alias ll="ls -l"
alias ls="ls -h --sort=version --group-directories-first --color"

alias rm="rm --one-file-system"

# Tar gZip Dir / Tar bzip2 (J) Dir
function tzd { tar czf "$1.tar.gz" "$1"; }
function tjd { tar cjf "$1.tar.bz2" "$1"; }


# Archlinux specific aliases/functions
function orphans
{
    if [[ ! -n $(pacman -Qdt) ]]; then
        echo no orphans to remove
    else
        sudo pacman -Rs $(pacman -Qdtq)
    fi
}
