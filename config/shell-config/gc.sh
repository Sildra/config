# gc.zsh
# A "git commit -m" that echo it to the stdout and can be redirected in a log
# file
# Realised by Thibault "Sildra" FRESNET
# Part of the Generic Makefile's system

function gc {
    if [ $# != 2 ]; then
        echo -e "\e[31mMust have 2 arguments\e[0m";
    else
        git commit -m "$1";
        if [ $? = 1 ]; then
            echo -e "\e[31mProblem while commiting\e[0m";
        else
            echo "\e[35m[COMMIT]\e[0m $1" >> $2;
            git add $2;
            git commit -m "Adding log file";
        fi
    fi
}
