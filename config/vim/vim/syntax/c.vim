syn match Type "\<\(t\|g\|f\)_\w*"
syn match Structure "\<\(s\|e\|u\)_\w*"
syn match Type "\w*_\(t\|g\|f\)\>"
syn match Structure "\w*_\(s\|e\|u\)\>"
