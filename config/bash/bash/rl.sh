# rl.sh
# Reload bash configuration
# Credits to Pierre "Delroth" BOURDON

function rl {
    hash -r
    source ~/.bashrc
}
