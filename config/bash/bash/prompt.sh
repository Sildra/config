# prompt.zsh
# My powerline 256C multi-line bash prompt.
# Made by Thibault 'Sildra' FRESNET
#
# Special thanks to Alexis 'Horgix' CHOTARD and the vim/powerline development
# team who greatly inspired me.

function _lookup_command()
{
	local cmd_idx=6
	local ppid_idx=3
	local lookup_command=$1
	local arr=($$ "nopr")
	while [ "`basename -- ${arr[1]}`" != "$lookup_command" ] && [ ${arr[0]} -ne 1 ]; do
		arr=(`ps -f -p ${arr[0]} | tail -n 1 | sed 's/^\s*//;s/\s+/ /g' | cut -d ' ' -f $ppid_idx,$cmd_idx`)
	done
	if [ "`basename -- ${arr[1]}`" != "$lookup_command" ]; then
		return 1
	else
		return 0
	fi
}


function _prompt_precmd ()
{
    # If we have a ssh connection on the process tree, we display the hostname
    if [ $__has_sshd -eq 0 ]; then
        # Background red
        local pr_h="$brd$fcl$reset$brd$fbk2 @\h $frd$bbk2$fcl$reset"
    else
        # Background orange
        local pr_h="$bo$fcl$reset$bo$fbk2 @\h $fo$bbk2$fcl$reset"
    fi

    local pr_u="$fgra$bdr \u $fdr"
    local pr_p="$fyl$bbk2 \w $ecl$reset"
    local pr_d="$fcy$bbk2 \D{%y-%m-%d %H:%M:%S} $ecl$reset"

    PROMPT_LEFT=$pr_u$pr_h$pr_p$pr_d
}

function prompt_set_256_colors ()
{
    # Resets all styles
    reset=$'\e[0m'

    fyl=$'\e[38;5;178m' # foreground yellow
    byl=$'\e[48;5;178m' # background yellow

    fo=$'\e[38;5;203m' # foreground orange
    bo=$'\e[48;5;203m' # background orange

    fvl=$'\e[38;5;177m' # foreground violet
    # bvl=$'\e[48;5;177m' # background violet

    fcy=$'\e[38;5;33m' # foreground cyan
    # bcy=$'\e[48;5;33m' # background cyan

    # If we have a substitued user, change user color
    _lookup_command su
    if [ $? -ne 0 ] || [ -z "$SUDO_USER" ]; then
        fdr=$'\e[38;5;52m' # foreground dark red
        bdr=$'\e[48;5;52m' # background dark red
    else
        fdr=$'\e[38;5;53m' # foreground dark red
        bdr=$'\e[48;5;53m' # background dark red
    fi

    frd=$'\e[38;5;196m' # foreground red
    brd=$'\e[48;5;196m' # background red

    fbk=$'\e[38;5;233m' # foreground black
    bbk=$'\e[48;5;233m' # background black

    fbk2=$'\e[38;5;234m' # foreground black2
    bbk2=$'\e[48;5;234m' # background black2

    fgra=$'\e[38;5;245m' # foreground gray
    bgra=$'\e[48;5;245m' # background gray

    fgr=$'\e[38;5;46m' # foreground green
    # bgr=$'\e[48;5;46m' # background green

    prompt_offset=22
}

function prompt_set_utf_8 ()
{
    # UTF-8 chevron chars :
    fcl=$(echo -e '\0342\0256\0200') # filled chevron left
    ecl=$(echo -e '\0342\0256\0201') # empty chevron left
    fcr=$(echo -e '\0342\0256\0202') # filled chevron right
    ecr=$(echo -e '\0342\0256\0203') # empty chevron right
}


function prompt_set_fancy ()
{
    # Fancy chevron chars :
    fcl="" # filled chevron left
    ecl=">" # empty chevron left
    fcr="" # filled chevron right
    ecr="<" # empty chevron right
}

function prompt_set_8_colors ()
{
    # Resets all styles
    reset=$'\e[0m'

    fyl=$'\e[33m' # foreground yellow
    byl=$'\e[43m' # background yellow

    fo=$'\e[33m' # foreground orange
    bo=$'\e[43m' # background orange

    fvl=$'\e[35m' # foreground violet
    # bvl=$'\e[48;5;177m' # background violet

    fcy=$'\e[36m' # foreground cyan
    # bcy=$'\e[48;5;33m' # background cyan

    fdr=$'\e[31m' # foreground dark red
    bdr=$'\e[41m' # background dark red

    frd=$'\e[31m' # foreground red
    brd=$'\e[41m' # background red

    fbk=$'\e[30m' # foreground black
    bbk= # background black

    fbk2=$'\e[30m' # foreground black2
    bbk2=$'\e[40m' # background black2

    fgra=$'\e[39m' # foreground gray
    bgra=$'\e[49m' # background gray

    fgr=$'\e[32m' # foreground green
    # bgr=$'\e[48;5;46m' # background green
}

function prompt_set_colors ()
{
    if [ "$terminfo[colors]" = 256 ]; then
        prompt_set_256_colors
    else
        prompt_set_8_colors
    fi
    prompt_set_fancy
}

function _prompt_command ()
{
    status=$?
    _status=""
    _status_color="$fgr"
    if [ $status -ne 0 ]; then
        _status="$status"
        _status_color="$frd"
    fi
}

function set_prompt ()
{
    export PS1="$PROMPT_LEFT
\["'$_status_color'"$bbk\]"'$_status'"\[$ecl$reset\] "
}

_lookup_command sshd
export HAS__SSHD=`echo $?`
export PROMPT_COMMAND=_prompt_command
prompt_set_colors
_prompt_precmd
set_prompt
