#!/bin/sh

# Toggle mute for alsa's Master volume
# By Thibault "Sildra" FRESNET

amixer get Master | grep "off" &> /dev/null
if [ $? = 0 ]; then
    amixer set Master unmute &> /dev/null
else
    amixer set Master mute &> /dev/null
fi
