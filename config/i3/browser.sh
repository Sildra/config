#!/bin/bash

# By Thibault "Sildra" FRESNET

browser=opera

i3-msg workspace web &> /dev/null
pgrep -x $browser &> /dev/null
if [ $? = 1 ]; then
    i3-msg "exec $browser &> /dev/null" &>/dev/null
fi
