#!/bin/bash

# Player managing system
# Try to connect to LAN MPD Server then WAN MPD Server then localhost
# Can send basic commands using the same scheme with ncmpcpp

# By Thibault "Sildra" FRESNET

# Those 2 vars are stored into the shell-config/perso.sh that have been
# excluded from the repository
HOST=$MPD_DISTANT_HOST
LHOST=$MPD_DISTANT_LHOST

if [ $# = 1 ]; then
    # BASIC COMMANDS
    ncmpcpp -h ${HOST} $1 &>/dev/null
    if [ $? = 1 ]; then
        ncmpcpp $1 &>/dev/null
    fi
else
    # Try to launch an ncmpcpp|cvlc workspace
    pgrep -f ncmpcpp &>/dev/null
    if [ $? = 1 ]; then
        ifconfig | grep 192.168.1.68 &>/dev/null
        if [ $? = 0 ]; then
            i3-msg "workspace player; exec urxvt -e ncmpcpp -h ${LHOST};
            workspace player" &>/dev/null
            sleep 1
        fi
    fi
    pgrep -f ncmpcpp &>/dev/null
    if [ $? = 1 ]; then
        i3-msg "workspace player; exec urxvt -e ncmpcpp -h ${HOST};
        workspace player" &>/dev/null
        sleep 1
    fi
    pgrep -f "ncmpcpp -h ${HOST}" &>/dev/null
    if [ $? = 0 ]; then
        pgrep -f vlc &>/dev/null
        if [ $? = 1 ]; then
            ncmpcpp -h ${HOST} play &> /dev/null
            i3-msg "workspace player; exec urxvt -e cvlc http://${HOST}:8000;
            workspace player" &>/dev/null
        fi
    fi
    pgrep -f ncmpcpp &>/dev/null
    if [ $? = 1 ]; then
        i3-msg "workspace player; exec urxvt -e ncmpcpp;
        workspace player" &>/dev/null
    fi
    i3-msg workspace player &>/dev/null
fi
