#!/bin/bash
# Go aliasing change directory system

# Author : Thibault "Sildra" FRESNET

mkdir -p $HOME/.gogo

if [ $# = 0 ]; then
    if [ -e $HOME/.gogo/! ]; then
        echo "$PWD" > $HOME/.gogo/-
        go=`cat $HOME/.gogo/!`
        cd $go
        echo "$PWD" > $HOME/.gogo/+
    else
        echo "Error : $HOME/.gogo/! does not exists"
    fi
fi

if [ $# = 1 ]; then
    case $1 in
        -r)
            rm -f $HOME/.gogo/- $HOME/.gogo/!
            ;;
        -a)
            echo "$PWD" > $HOME/.gogo/!
            ;;
        -l)
            for file in $HOME/.gogo/*; do
                filen=$(basename $file)
                echo -ne `printf $filen | sed "s/\([-+!]\)/\\\\\\\\e[31m\1\\\\\\\\e[0m/"`
                filen=${#filen}
                while [ $filen -lt 10 ]; do
                    printf " "
                    filen=$(($filen + 1))
                done
                printf " : "
                file=`cat $file`
                printf ${file/#$HOME/\\e[34m\~\\e[0m}\\n
            done
            ;;
        -h)
            echo "Usage : $0 -[halr] [alias]"
            echo "    alias : perform action on alias (default is cd)"
            echo
            echo "    -h : display help (this message)"
            echo "    -l : list aliases"
            echo "    -a [alias] : add alias"
            echo "    -r [alias] : remove alias"
            ;;
        *)
            if [ -e $HOME/.gogo/$@ ]; then
                go=`cat $HOME/.gogo/$@`
                echo "$PWD" > $HOME/.gogo/-
                cd $go
                echo "$PWD" > $HOME/.gogo/+
            else
                echo "Error : $HOME/.gogo/$@ does not exists"
            fi
    esac
fi

if [ $# = 2 ]; then
    case $1 in
        -a)
            echo "$PWD" > $HOME/.gogo/$2
            ;;
        -r)
            rm $HOME/.gogo/$2
            ;;
        *)
            echo "Option $1 does not exists, see usage with -h"
    esac
fi

unset filen
unset file

